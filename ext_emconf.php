<?php

$EM_CONF[$_EXTKEY] = [
	'title' => 'KDN Template',
	'description' => 'Custom template settings for KDN website',
	'category' => 'misc',
	'author' => 'Gert Hammes',
	'author_email' => 'info@gerthammes.de',
	'author_company' => '',
	'shy' => '',
	'priority' => '',
	'module' => '',
	'state' => 'stable',
	'internal' => '',
	'uploadfolder' => '0',
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'version' => '1.0.0',
	'constraints' => [
		'depends' => [
            'typo3' => '9.5.0-9.5.99',
            'citko_config' => '1.0.0-9.9.99',
            'citko_contact' => '0.1.1-9.9.99',
        ],
		'conflicts' => [
        ],
		'suggests' => [
        ],
    ],
    'autoload' => array(
        'psr-4' => array('KDN\\KdnTemplate\\' => 'Classes')
    ),
];
