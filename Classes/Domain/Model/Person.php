<?php
namespace KDN\KdnTemplate\Domain\Model;

use Citkomm\CitkoContact\Domain\Model\Person as BasePerson;

/**
 * Person
 */
class Person extends BasePerson {

    /**
     * image
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected $qrcodeImage = NULL;

    /**
     * Returns the qrcode image
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $qrcodeImage
     */
    public function getQrcodeImage() {
        return $this->qrcodeImage;
    }

    /**
     * Sets the qrcode image
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $qrcodeImage
     * @return void
     */
    public function setQrcodeImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $qrcodeImage) {
        $this->qrcodeImage = $qrcodeImage;
    }
}