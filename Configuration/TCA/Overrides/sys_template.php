<?php
defined('TYPO3_MODE') or die();

// add TYPOSCRIPT template
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('kdn_template', 'Configuration/TypoScript', 'KDN custom template');