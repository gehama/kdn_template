<?php
defined('TYPO3_MODE') || die();

$tempColumns = array(
    'qrcode_image' => array(
        'exclude' => 1,
        'label' => 'LLL:EXT:kdn_template/Resources/Private/Language/locallang_db.xlf:tx_citkocontact_domain_model_person.qrcode_image',
        'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
            'qrcode_image',
            array('maxitems' => 1),
            $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
        ),
    ),

);


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tx_citkocontact_domain_model_person', $tempColumns);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'tx_citkocontact_domain_model_person',
    'qrcode_image',
    '',
    'after:answer'
);